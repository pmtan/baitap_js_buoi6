// Bai 1: Tim So Nguyen n
function timSoNguyenN(){
    var max = document.getElementById("max").value * 1;
    var sum = 0;
    var n = 0;
    while (sum < max){
        n++;
        sum +=n;
    }
    console.log(sum);
    console.log(n);
    document.getElementById("bai-1-result").innerHTML = `<p>Số nguyên dương nhỏ nhất: ${n} </p>`;
}
// Bai 2: Tinh Tong
function tinhTong(){
    var x = document.getElementById("x").value * 1;
    var n = document.getElementById("n").value * 1;
    sum = 0;
    for (i = 1; i<= n; i++){
        sum = sum + Math.pow(x,i);
    }
    console.log(sum);
    document.getElementById("bai-2-result").innerHTML = `<p>Kết Quả: ${sum} </p>`;
}

// Bai 3: Tinh Giai Thua
function tinhGiaiThua(){
    var n = document.getElementById("n-giai-thua").value * 1;
    var giaiThua = 1;
    for (i = 1; i <= n; i++){
        giaiThua*=i;
    }
    console.log(giaiThua);
    document.getElementById("bai-3-result").innerHTML = `<p>Kết Quả: ${giaiThua} </p>`;

}

// Bai 4: Tao Div
function taoDiv(){
    for(i = 1; i <= 10; i++){
        if (i%2 == 0){
            var div = document.createElement("div");
                div.style.height = "30px";
                div.style.background = "red";
                div.style.color = "white";
                div.innerHTML = `<p>Div chẵn ${i} </p>`;
                document.getElementById("bai-4-result").appendChild(div);
        } else {
            var div = document.createElement("div");
                div.style.height = "30px";
                div.style.background = "blue";
                div.style.color = "white";
                div.innerHTML = `<p>Div lẻ ${i} </p>`;
                document.getElementById("bai-4-result").appendChild(div);
        }
    }
}